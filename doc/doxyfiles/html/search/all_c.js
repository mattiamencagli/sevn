var searchData=
[
  ['labels_5fstarmatrix_0',['labels_STARMATRIX',['../class_i_o.html#a5cf54d071f2b9250e88bf6d78a94328c',1,'IO']]],
  ['lambda_1',['Lambda',['../class_lambda.html',1,'']]],
  ['lambda_2',['lambda',['../class_common_envelope.html#a7da0c04854edd7c078cc843f857575f5',1,'CommonEnvelope']]],
  ['lambda_3',['Lambda',['../class_lambda.html#a566fbe140e17276968e3c1cd5cb6f3f1',1,'Lambda']]],
  ['lambda_5fagb_4',['lambda_AGB',['../class_lambda___nanjing.html#ac67837a70ee4dc2e826124d75a09fc07',1,'Lambda_Nanjing::lambda_AGB(_UNUSED double Mzams, _UNUSED double Z, _UNUSED double Radius, _UNUSED double Menv, _UNUSED double lambda_th)'],['../class_lambda___nanjing.html#afa8e83aa824581e372a55e772dfceffc',1,'Lambda_Nanjing::lambda_AGB(const Star *s)']]],
  ['lambda_5fbase_5',['Lambda_Base',['../class_lambda___base.html#a5790b8a55b14072c9de4e9069298fb2c',1,'Lambda_Base::Lambda_Base()'],['../class_lambda___base.html',1,'Lambda_Base'],['../class_lambda___base.html#a6488c72985469eb961eaef86a09c95af',1,'Lambda_Base::Lambda_Base(_UNUSED const Star *s)'],['../class_lambda___base.html#ae26982951800961e2e02c13d9e5e8391',1,'Lambda_Base::Lambda_Base(_UNUSED const IO *io)']]],
  ['lambda_5fbase_2eh_6',['lambda_base.h',['../lambda__base_8h.html',1,'']]],
  ['lambda_5fbase_5fptr_7',['lambda_base_ptr',['../class_lambda.html#a2739fabe5e8a4e0e85572073a13d8782',1,'Lambda']]],
  ['lambda_5fcheb_8',['lambda_Cheb',['../class_lambda___nanjing.html#a626e0e07ad9e8745ff9a0e27c41bedcd',1,'Lambda_Nanjing::lambda_Cheb(const Star *s)'],['../class_lambda___nanjing.html#a962a0190d056ac1cfc458607302e177e',1,'Lambda_Nanjing::lambda_Cheb(_UNUSED double Mzams, _UNUSED double Z, _UNUSED double Radius, _UNUSED double Menv, _UNUSED double lambda_th)']]],
  ['lambda_5festimate_9',['lambda_estimate',['../class_lambda___nanjing.html#a7734dc5a204286559c48b92e3963fbb2',1,'Lambda_Nanjing']]],
  ['lambda_5fgiant_10',['lambda_Giant',['../class_lambda___nanjing.html#a4b72a0e8e1142368e34848375be4f9a0',1,'Lambda_Nanjing::lambda_Giant(const Star *s)'],['../class_lambda___nanjing.html#a5612a52a04191c6aa3f91f366ec560d2',1,'Lambda_Nanjing::lambda_Giant(_UNUSED double Mzams, _UNUSED double Z, _UNUSED double Radius, _UNUSED double Menv, _UNUSED double lambda_th)']]],
  ['lambda_5fklencki_11',['Lambda_Klencki',['../class_lambda___klencki.html',1,'Lambda_Klencki'],['../class_lambda___klencki.html#a73d9423b15cda6d226915ac114743582',1,'Lambda_Klencki::Lambda_Klencki(const Star *s)'],['../class_lambda___klencki.html#a73afd370a22fd7cbc01d7dc6fda0cc55',1,'Lambda_Klencki::Lambda_Klencki(const IO *io)']]],
  ['lambda_5fklencki21_2ecpp_12',['lambda_klencki21.cpp',['../lambda__klencki21_8cpp.html',1,'']]],
  ['lambda_5fklencki21_2eh_13',['lambda_klencki21.h',['../lambda__klencki21_8h.html',1,'']]],
  ['lambda_5fklencki_5finterpolator_14',['Lambda_Klencki_interpolator',['../class_lambda___klencki__interpolator.html',1,'Lambda_Klencki_interpolator'],['../class_lambda___klencki__interpolator.html#aa99c0459bcce4ab352885b726dcec827',1,'Lambda_Klencki_interpolator::Lambda_Klencki_interpolator(const Star *s)'],['../class_lambda___klencki__interpolator.html#ad2ed15cdaa24b6ede77c1e01f46bb57c',1,'Lambda_Klencki_interpolator::Lambda_Klencki_interpolator(const IO *io)']]],
  ['lambda_5fnanjing_15',['Lambda_Nanjing',['../class_lambda___nanjing.html',1,'Lambda_Nanjing'],['../class_lambda___nanjing.html#a141f94fc62a871d1150384786da14ea9',1,'Lambda_Nanjing::Lambda_Nanjing(_UNUSED const IO *io)'],['../class_lambda___nanjing.html#ac660046f783586326626a520fecb87ce',1,'Lambda_Nanjing::Lambda_Nanjing()'],['../class_lambda___nanjing.html#ae024840ea896ed423aade415cb74a219',1,'Lambda_Nanjing::Lambda_Nanjing(_UNUSED const Star *s)']]],
  ['lambda_5fnanjing_2ecpp_16',['lambda_nanjing.cpp',['../lambda__nanjing_8cpp.html',1,'']]],
  ['lambda_5fnanjing_2eh_17',['lambda_nanjing.h',['../lambda__nanjing_8h.html',1,'']]],
  ['lambda_5fnanjing_5finterpolator_18',['Lambda_Nanjing_interpolator',['../class_lambda___nanjing__interpolator.html#a82c2d2159bd35480c0b61139f1be8411',1,'Lambda_Nanjing_interpolator::Lambda_Nanjing_interpolator(_UNUSED const IO *io)'],['../class_lambda___nanjing__interpolator.html#aefe5015297c45381fc55f83a4ceab546',1,'Lambda_Nanjing_interpolator::Lambda_Nanjing_interpolator(_UNUSED const Star *s)'],['../class_lambda___nanjing__interpolator.html',1,'Lambda_Nanjing_interpolator'],['../class_lambda___nanjing__interpolator.html#a86cb47d635568a4af0cf7508edd0daab',1,'Lambda_Nanjing_interpolator::Lambda_Nanjing_interpolator()']]],
  ['lambda_5fpurehe_19',['lambda_pureHe',['../class_lambda___nanjing.html#a759feb471f1bdbe0393427ed41d310e0',1,'Lambda_Nanjing::lambda_pureHe(_UNUSED double Mzams, _UNUSED double Z, _UNUSED double Radius, _UNUSED double Menv, _UNUSED double lambda_th)'],['../class_lambda___nanjing.html#a76caa80e76185de504348ed2ec7430d2',1,'Lambda_Nanjing::lambda_pureHe(const Star *s)']]],
  ['lambdabg_20',['lambdaBG',['../class_lambda___nanjing.html#af4914b8114ac860589339f44ad5c310b',1,'Lambda_Nanjing']]],
  ['large_21',['LARGE',['../namespaceutilities.html#af6ff0e3a92481bc39762efddcefcc111',1,'utilities']]],
  ['last_5fevolve_5fnumber_22',['last_evolve_number',['../class_j_i_t___property.html#afef004ca5a959ba75c7bb20661764219',1,'JIT_Property']]],
  ['limit_5fand_5fcorrect_5fmass_5ftransfer_5ffor_5fdonor_5ffrom_5fbinary_23',['limit_and_correct_mass_transfer_for_donor_from_binary',['../class_binstar.html#aa455558e6e1e3c834e3c8062bd8de7e0',1,'Binstar']]],
  ['list_5fcols_5fbinary_24',['list_cols_binary',['../class_i_o.html#abcc7edabe3d0811bc46a9c68b8e8b6ec',1,'IO']]],
  ['list_5fcols_5fstar_25',['list_cols_star',['../class_i_o.html#adafa7aa9fc3a50f0a7e32524ee533304',1,'IO']]],
  ['list_5ffile_26',['list_file',['../class_i_o.html#a64bbe89ff4b744049da60893f42ba256',1,'IO']]],
  ['listgenerator_27',['ListGenerator',['../classutilities_1_1_list_generator.html#ad387d3f6653373fbc176d06967433c96',1,'utilities::ListGenerator::ListGenerator()'],['../classutilities_1_1_list_generator.html#a86ee7bc0e5a30992aff1e7e6b11bac90',1,'utilities::ListGenerator::ListGenerator(double _vstep, double _vstep_max=std::nan(&quot;&quot;), double _vstep_min=std::nan(&quot;&quot;))'],['../classutilities_1_1_list_generator.html#acc5bfae58cd052935b4e603bd4489897',1,'utilities::ListGenerator::ListGenerator(const std::vector&lt; double &gt; &amp;_tlist)'],['../classutilities_1_1_list_generator.html',1,'utilities::ListGenerator']]],
  ['liststars_28',['liststars',['../class_i_o.html#a77a375b43764c0bf47b620f8a4a13d47',1,'IO']]],
  ['literal_29',['literal',['../namespace_lookup.html#adbd458568472f110b7a549b0f6f070b5',1,'Lookup::literal(Phases A)'],['../namespace_lookup.html#a2087bfd092bcd10205bb154f86c5ab64',1,'Lookup::literal(Remnants A)'],['../namespace_lookup.html#af9ac561ce1241c65ccb0c197d3081302',1,'Lookup::literal(EventsList A)']]],
  ['load_30',['load',['../class_i_o.html#a6f56823f20c05fc9b7fb74b27c28d963',1,'IO::load()'],['../class_s_e_v_npar.html#a5e5a4155f090718bfabd650454a9ea50',1,'SEVNpar::load()']]],
  ['load_5fauxiliary_5ftable_31',['load_auxiliary_table',['../class_i_o.html#a4723148152bb5e16b1e85a271c6506ac',1,'IO::load_auxiliary_table()'],['../class_star.html#a879fe2217463f6686b23fbbc2f604d91',1,'Star::load_auxiliary_table()']]],
  ['load_5fstars_32',['load_stars',['../class_i_o.html#a8208e360fe14ca4f4d35f258ad8e6963',1,'IO']]],
  ['load_5ftable_33',['load_table',['../classcompactness.html#a0c5084cee44fea66dadc9ec18da51737',1,'compactness::load_table()'],['../class_death_matrix.html#ac964b0b442e1da24a74b3b0ee6073f49',1,'DeathMatrix::load_table()']]],
  ['load_5ftables_34',['load_tables',['../class_i_o.html#a35556aa0b6afcaf09a97d82767040898',1,'IO']]],
  ['localtime_35',['Localtime',['../class_localtime.html',1,'Localtime'],['../class_localtime.html#ab3633c8002ac35837a1a68f668c14ed2',1,'Localtime::Localtime()']]],
  ['log_36',['log',['../classsevnstd_1_1_sevn_logging.html#affcf3939810e71cacac9c1b3508040dd',1,'sevnstd::SevnLogging']]],
  ['log_5flevel_37',['log_level',['../classsevnstd_1_1_sevn_logging.html#a78203c55c2023fce47a8d6abde21bf3f',1,'sevnstd::SevnLogging']]],
  ['log_5fmess_38',['log_mess',['../class_common_envelope.html#a7cc21181381bf622bfacd8e3dc4e1bc7',1,'CommonEnvelope']]],
  ['log_5fmess_5fconaked_39',['log_mess_COnaked',['../class_star.html#a32b61426dce05d994cc4d63ef6edb0a0',1,'Star']]],
  ['log_5fmess_5fhenaked_40',['log_mess_HEnaked',['../class_star.html#aa06dfb0fcfe58b7d83bd9f02e70eda20',1,'Star']]],
  ['log_5fmess_5fjumped_41',['log_mess_jumped',['../class_star.html#a5b14627e2d3265e657aa94035c096aac',1,'Star']]],
  ['log_5fmessage_42',['log_message',['../class_s_n_kicks.html#a83a38291fe5bc575a2542d9e399ab3e1',1,'SNKicks::log_message()'],['../class_mix.html#aa6267e06bf9aa52fc66043fb6cd6fec2',1,'Mix::log_message()'],['../class_kollision.html#a2d6f92bd7624f1407cb30f656f52d630',1,'Kollision::log_message()'],['../class_hardening.html#abf292b257f892a75f2c95be9dd66cb36',1,'Hardening::log_message()']]],
  ['log_5fmessage_5fcirc_43',['log_message_circ',['../class_standard_circularisation.html#aa5dcce686e0216d29afb0131edd64812',1,'StandardCircularisation']]],
  ['log_5fmessage_5fend_44',['log_message_end',['../class_roche_lobe.html#a28064ec068d57fdfa1c40971cd1b63d4',1,'RocheLobe']]],
  ['log_5fmessage_5fsnia_45',['log_message_SNIa',['../class_star.html#aa09ea3c73080bdef8e51a870bdf12a65',1,'Star']]],
  ['log_5fmessage_5fstart_46',['log_message_start',['../class_roche_lobe.html#a1d543267e3cdce6342e2bb901de94996',1,'RocheLobe']]],
  ['log_5fmessage_5fswallowed_47',['log_message_swallowed',['../class_common_envelope.html#a1a51a8f8e368a60d54db793e5738f5e5',1,'CommonEnvelope::log_message_swallowed()'],['../class_roche_lobe.html#a6f7141affc375365197d116a60e32123',1,'RocheLobe::log_message_swallowed(Binstar *binstar, Star *swallowed, Star *other, double DM_accreted)'],['../class_roche_lobe.html#a94cb5eb3923b2466008add1962d89022',1,'RocheLobe::log_message_swallowed(Binstar *binstar, double DM_accreted)']]],
  ['log_5fprint_48',['log_print',['../namespaceutilities.html#ac85dd844bb2a32af1c1f2ac25b3ef3f2',1,'utilities::log_print(const std::string &amp;label, Binstar *binstar, ListP... args)'],['../namespaceutilities.html#aa3c6140e1ae352197908cadd70fe3ec3',1,'utilities::log_print(const std::string &amp;label, Star *star, ListP... args)']]],
  ['log_5fput_49',['log_put',['../class_i_o.html#a7e37248890d47aaf46fc0a32c9758187',1,'IO']]],
  ['log_5fstar_5finfo_50',['log_star_info',['../namespaceutilities.html#ad4595aef0bc3c65e955fa3a9bf23b9c8',1,'utilities']]],
  ['logfile_51',['logfile',['../class_i_o.html#a897148662639fad729bdf1bd1787d696',1,'IO']]],
  ['logstring_52',['logstring',['../class_i_o.html#a6ddb82c59853fd8707ba7e1e2002adaa',1,'IO']]],
  ['lookup_53',['Lookup',['../namespace_lookup.html',1,'']]],
  ['lookup_5fand_5fphases_2ecpp_54',['lookup_and_phases.cpp',['../lookup__and__phases_8cpp.html',1,'']]],
  ['lookup_5fand_5fphases_2eh_55',['lookup_and_phases.h',['../lookup__and__phases_8h.html',1,'']]],
  ['lookuppositions_56',['lookuppositions',['../class_star.html#a7caed3e34d35800d094f1a8f61e2c3cf',1,'Star']]],
  ['lose_5fthe_5fenvelope_57',['lose_the_envelope',['../class_star.html#a72893aea19e81e3151adbbac538e6b39',1,'Star::lose_the_envelope()'],['../class_common_envelope.html#a6e99a2fda4065f9a5907b44e4d13ff35',1,'CommonEnvelope::lose_the_envelope()']]],
  ['lsun_5fto_5fsolar_58',['LSun_to_Solar',['../namespaceutilities.html#a975b6ee31de7176975ea5b8cfd7fcc2b',1,'utilities']]],
  ['luminosity_59',['Luminosity',['../class_n_srem.html#a7876f9caa53d61bccc10515b5145316e',1,'NSrem::Luminosity()'],['../class_w_drem.html#a1f33da42e741238e7b581b5bf40f0d86',1,'WDrem::Luminosity()'],['../class_staremnant.html#a2b435815f42c861010d73a9bca4112e4',1,'Staremnant::Luminosity()'],['../class_luminosity.html#a1b7d63d9e936438b34d5bf9c2a4bb046',1,'Luminosity::Luminosity()'],['../class_b_hrem.html#ae99c7edfb86c5a94f486c88d620c5483',1,'BHrem::Luminosity()'],['../class_luminosity.html',1,'Luminosity']]]
];
