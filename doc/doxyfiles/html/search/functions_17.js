var searchData=
[
  ['xspin_0',['Xspin',['../class_xspin.html#a297db2c1c10b552fb7253dbc44df0032',1,'Xspin::Xspin()'],['../class_staremnant.html#a00a2981c7327990d34b58b01eb493787',1,'Staremnant::Xspin()'],['../class_b_hrem.html#ad1802f0bf19368bd23779c5a19be8c8e',1,'BHrem::Xspin()'],['../class_n_srem.html#a79acaddc62ce21530911dd15d5543b82',1,'NSrem::Xspin()'],['../class_w_drem.html#a65e4397948b0c83f8c8b44aed3b7ae4f',1,'WDrem::Xspin()']]],
  ['xspinaccretion_1',['XspinAccretion',['../class_b_hrem.html#a0a5ce27a0fc42e25bc96a5f02b117ce9',1,'BHrem']]],
  ['xspinfuller_2',['XspinFuller',['../class_b_hrem.html#a245e695a51018a83c0a648cf331ae8f4',1,'BHrem']]],
  ['xspingeneva_3',['XspinGeneva',['../class_b_hrem.html#a2fc9babe34c201d857455adcf253737a',1,'BHrem']]],
  ['xspinmaxwellian_4',['XspinMaxwellian',['../class_b_hrem.html#a73cb8bb98c23f6daade795cb58e82b3c',1,'BHrem']]],
  ['xspinmesa_5',['XspinMESA',['../class_b_hrem.html#a07dfc353df2f32fb42a9724d47c88b42',1,'BHrem']]],
  ['xspinzeros_6',['XspinZeros',['../class_b_hrem.html#a3a03faf7555437763406c4e61e4f2898',1,'BHrem']]]
];
